import axios from "../utilities/axiosCustomize";

const postCreateNewUsers = (email, password, username, role, image) => {
   let data = new FormData();
   data.append("email", email);
   data.append("password", password);
   data.append("username", username);
   data.append("role", role);
   data.append("userImage", image);

   return axios.post("/api/v1/participant", data);
};

const getAllUsers = () => {
   return axios.get("/api/v1/participant/all");
};

const updateUsers = (id, username, role, image) => {
   let data = new FormData();
   data.append("id", id);
   data.append("username", username);
   data.append("role", role);
   data.append("userImage", image);

   return axios.put("/api/v1/participant", data);
};

const deleteUsers = (userId) => {
   return axios.delete("/api/v1/participant", { data: { id: userId } });
};

const getListUsersWithPaginate = (page, limit) => {
   return axios.get(`/api/v1/participant?page=${page}&limit=${limit}`);
};

const postLogin = (email, password) => {
   return axios.post(`/api/v1/login`, { email, password });
};

const postRegister = (email, password, username) => {
   return axios.post(`/api/v1/register`, { email, password, username });
};

const getQuizByUser = () => {
   return axios.get(`/api/v1/quiz-by-participant`);
};

const getQuestionsById = (quizId) => {
   return axios.get(`/api/v1/questions-by-quiz?quizId=${quizId}`);
};

const postSubmitQuiz = (data) => {
   return axios.post(`/api/v1/quiz-submit`, { ...data });
};

const postCreateNewQuiz = (description, name, difficulty, quizImage) => {
   let data = new FormData();
   data.append("description", description);
   data.append("name", name);
   data.append("difficulty", difficulty);
   data.append("quizImage", quizImage);
   return axios.post(`/api/v1/quiz`, data);
};

const getAllQuizForAdmin = () => {
   return axios.get(`/api/v1/quiz/all`);
};

export {
   postCreateNewUsers,
   getAllUsers,
   updateUsers,
   deleteUsers,
   getListUsersWithPaginate,
   postLogin,
   postRegister,
   getQuizByUser,
   getQuestionsById,
   postCreateNewQuiz,
   postSubmitQuiz,
   getAllQuizForAdmin,
};
