import { useState } from "react";
import "../Auth/Login.scss";
import { useNavigate } from "react-router-dom";
import { postLogin } from "../../services/apiServices";
import { Bounce, ToastContainer, toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { doLogin } from "../../redux/action/userAction";
import { ImSpinner9 } from "react-icons/im";

const Login = (props) => {
   const navigate = useNavigate();
   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");
   const [isLoading, setIsLoading] = useState(false);
   const dispatch = useDispatch();

   const handleGoToHomespace = () => {
      navigate("/");
   };

   const handleGoToSignUp = () => {
      navigate("/register");
   };

   const validateEmail = (email) => {
      return String(email)
         .toLowerCase()
         .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
         );
   };

   const handleLogin = async () => {
      //validate
      let isValidEmail = validateEmail(email);
      if (!isValidEmail) {
         toast.error("Invalid email");
         return;
      }
      if (!password) {
         toast.error("Invalid password");
         return;
      }

      setIsLoading(true);

      // submit api
      let res = await postLogin(email, password);
      if (res && res.EC === 0) {
         dispatch(doLogin(res));
         toast.success(res.EM);
         setIsLoading(false);
         navigate("/");
      }
      if (res && res.EC !== 0) {
         toast.error(res.EM);
         setIsLoading(false);
      }
   };

   return (
      <div className="login-container">
         <div className="header">
            <span>Don't have an account yet?</span>
            <button onClick={() => handleGoToSignUp()}>Sign up</button>
         </div>
         <div className="title col-4 mx-auto">HoidanIT&Eric</div>
         <div className="welcome col-4 mx-auto">Hello, who's this?</div>
         <div className="content-form col-4 mx-auto">
            <div className="form-group">
               <label>Email</label>
               <input
                  type="email"
                  className="form-control"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
               />
            </div>
            <div className="form-group">
               <label>Pasword</label>
               <input
                  type="password"
                  className="form-control"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
               />
            </div>
            <span>Forgot pasword?</span>
            <div className="btn-login">
               <button disabled={isLoading} onClick={() => handleLogin()}>
                  {isLoading === true ? <ImSpinner9 className="loader-icon" /> : ""}

                  <span>Login to HoiDanIT</span>
               </button>
            </div>
            <div className="text-center " role="button">
               <span onClick={() => handleGoToHomespace()}>&#60;&#60; Go to homespace</span>
            </div>
         </div>
      </div>
   );
};

export default Login;
