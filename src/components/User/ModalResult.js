import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

const ModalResult = (props) => {
   const { showModalResult, setShowModalResult, dataModalResult } = props;
   console.log(dataModalResult);

   const handleClose = () => setShowModalResult(false);
   const handleShow = () => setShowModalResult(true);

   return (
      <>
         <Modal show={showModalResult} onHide={handleClose} backdrop="static">
            <Modal.Header closeButton>
               <Modal.Title>Your Result</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <div>
                  Total Questions: <b>{dataModalResult.countTotal}</b>
               </div>
               <div>
                  Total Correct Answer: <b>{dataModalResult.countCorrect}</b>
               </div>
               <div></div>
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={handleClose}>
                  Show answer
               </Button>
               <Button
                  variant="primary"
                  onClick={() => {
                     handleClose();
                  }}
               >
                  Close
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
};

export default ModalResult;
