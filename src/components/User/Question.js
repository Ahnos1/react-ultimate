import _ from "lodash";

const Question = (props) => {
   const { data, currentQuestion } = props;
   if (_.isEmpty(data)) {
      return <></>;
   }

   const handleHandleCheckbox = (event, answerId, questionId) => {
      // console.log(">>>Check checkbox", event.target.checked);
      // console.log("data props: ", data, "id: ", answerId, questionId);
      props.handleCheckbox(answerId, questionId);
   };

   return (
      <>
         {data.image ? (
            <div className="question-image">
               <img src={`data:image/png;base64,${data.image}`} />
            </div>
         ) : (
            <div className="question-image"></div>
         )}
         <div className="question">
            Question {currentQuestion + 1}: {data.questionDescription} ?
         </div>
         <div className="answer">
            {data.answer &&
               data.answer.length > 0 &&
               data.answer.map((answer, index) => {
                  return (
                     <div key={`answer-${index}`} className="a-child">
                        <div className="form-check">
                           <input
                              className="form-check-input"
                              type="checkbox"
                              checked={answer.isSelected}
                              onChange={(event) =>
                                 handleHandleCheckbox(event, answer.id, data.questionId)
                              }
                           />
                           <label className="form-check-label">{answer.description}</label>
                        </div>
                     </div>
                  );
               })}
         </div>
      </>
   );
};

export default Question;
