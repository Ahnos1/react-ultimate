import { useEffect, useState } from "react";
import { useParams, useLocation } from "react-router-dom";
import { getQuestionsById, postSubmitQuiz } from "../../services/apiServices";
import _ from "lodash";
import "./DetailQuiz.scss";
import Question from "../User/Question";
import ModalResult from "./ModalResult";

const DetailQuiz = (props) => {
   const params = useParams();
   const quizId = params.id;
   const location = useLocation();
   const [dataQuestion, setDataQuestion] = useState([]);
   const [currentQuestion, setCurrentQuestion] = useState(0);
   const [showModalResult, setShowModalResult] = useState(false);
   const [dataModalResult, setDataModalResult] = useState({});

   useEffect(() => {
      fetchQuestions();
   }, [quizId]);

   const fetchQuestions = async () => {
      let res = await getQuestionsById(quizId);
      if (res && res.EC === 0) {
         let raw = res.DT;
         let data = _.chain(raw)
            .groupBy("id")
            // `key` is group's name (color), `value` is the array of objects
            .map((value, key) => {
               let answer = [];
               let questionDescription,
                  image = null;
               value.forEach((item, index) => {
                  if (index === 0) {
                     questionDescription = item.description;
                     image = item.image;
                  }
                  item.answers.isSelected = false;
                  answer.push(item.answers);
               });
               return { questionId: key, answer, questionDescription, image };
            })
            .value();
         setDataQuestion(data);
      }
   };

   const handleNext = () => {
      if (dataQuestion && dataQuestion.length > currentQuestion + 1) {
         setCurrentQuestion(currentQuestion + 1);
      }
   };

   const handlePrev = () => {
      if (currentQuestion - 1 < 0) {
         return;
      }
      setCurrentQuestion(currentQuestion - 1);
   };

   const handleCheckbox = (answerId, questionId) => {
      let dataQuestionClone = _.cloneDeep(dataQuestion);
      let question = dataQuestionClone.find((item) => +item.questionId === +questionId);
      if (question && question.answer) {
         question.answer = question.answer.map((item) => {
            if (+item.id === +answerId) {
               item.isSelected = !item.isSelected;
            }
            return item;
         });
      }
      let index = dataQuestionClone.findIndex((item) => +item.questionId === +questionId);
      if (index > -1) {
         dataQuestionClone[index] = question;
         setDataQuestion(dataQuestionClone);
      }
   };

   const handleFinishQuiz = async () => {
      // console.log("Check data: ", dataQuestion);
      let payload = { quizId: +quizId, answer: [] };
      let answers = [];
      if (dataQuestion && dataQuestion.length > 0) {
         dataQuestion.forEach((question) => {
            let questionId = question.questionId;
            let userAnswerId = [];

            question.answer.forEach((answer) => {
               if (answer.isSelected === true) {
                  userAnswerId.push(answer.id);
               }
            });

            answers.push({
               questionId: +questionId,
               userAnswerId,
            });
         });
         payload.answers = answers;
         // console.log(">>Final payload: ", payload);
         let res = await postSubmitQuiz(payload);
         if (res && res.EC === 0) {
            setDataModalResult({
               countCorrect: res.DT.countCorrect,
               countTotal: res.DT.countTotal,
               quizData: res.DT.quizData,
            });
            setShowModalResult(true);
         } else {
            alert("something's wrong");
         }
      }
   };

   return (
      <div className="detail-quiz-container">
         <div className="detail-quiz-left-content">
            <div className="title">
               Quiz {quizId}: {location?.state?.quizTilte}
            </div>
            <hr />
            <div className="question-body"></div>
            <div className="question-content">
               <Question
                  currentQuestion={currentQuestion}
                  data={
                     dataQuestion && dataQuestion.length > 0 ? dataQuestion[currentQuestion] : []
                  }
                  handleCheckbox={handleCheckbox}
               />
            </div>
            <div className="footer">
               <button className="btn btn-secondary" onClick={() => handlePrev()}>
                  Previous
               </button>
               <button className="btn btn-primary" onClick={() => handleNext()}>
                  Next
               </button>
               <button className="btn btn-warning" onClick={() => handleFinishQuiz()}>
                  Finish
               </button>
            </div>
         </div>
         <ModalResult
            showModalResult={showModalResult}
            setShowModalResult={setShowModalResult}
            dataModalResult={dataModalResult}
         />
         <div className="detail-quiz-right-content">count down</div>
      </div>
   );
};

export default DetailQuiz;
