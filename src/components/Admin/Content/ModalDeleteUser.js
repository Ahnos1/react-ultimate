import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { deleteUsers } from "../../../services/apiServices";
import { toast } from "react-toastify";

const ModalDeleteUser = (props) => {
   const { show, setShow, dataDelete } = props;

   const handleClose = () => setShow(false);
   const handleShow = () => setShow(true);

   const handleSubmitDeleteUser = async () => {
      let res = await deleteUsers(dataDelete.id);
      if (res && res.EC === 0) {
         toast.success(res.EM);
         handleClose();
         // await props.fetchAllUsers();
         props.setCurrentPage(1);
         await props.fetchUsersWithPaginate(1);
      }
      if (res && res.EC !== 0) {
         toast.error(res.EM);
      }
      //   handleClose();
   };

   return (
      <>
         <Modal show={show} onHide={handleClose} backdrop="static">
            <Modal.Header closeButton>
               <Modal.Title>Delete User</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               Are you sure to delete this user. Email:{" "}
               <b>{dataDelete && dataDelete.email ? dataDelete.email : ""}</b>
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={handleClose}>
                  Close
               </Button>
               <Button
                  variant="primary"
                  onClick={() => {
                     handleSubmitDeleteUser();
                  }}
               >
                  Delete{" "}
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
};

export default ModalDeleteUser;
