import { useState } from "react";
import "./ManageQuizzes.scss";
import Select from "react-select";
import { postCreateNewQuiz } from "../../../../services/apiServices";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import TableQuiz from "./TableQuiz";
import Accordion from "react-bootstrap/Accordion";

const ManageQuizzes = () => {
   const navigate = useNavigate();
   const [name, setName] = useState("");
   const [description, setDescription] = useState("");
   const [type, setType] = useState("");
   const [image, setImage] = useState(null);

   const options = [
      { value: "EASY", label: "EASY" },
      { value: "MEDIUM", label: "MEDIUM" },
      { value: "HARD", label: "HARD" },
   ];

   const handleUploadImage = (event) => {
      if (event.target && event.target.files && event.target.files[0]) {
         setImage(event.target.files[0]);
      }
   };

   const handleAddQuizBtn = async () => {
      if (!name || !description) {
         toast.error("Name/Description must be filled");
         return;
      }
      let res = await postCreateNewQuiz(description, name, type?.value, image);
      if (res && res.EC === 0) {
         toast.success(res.EM);
         setName("");
         setDescription("");
         setType("");
         setImage(null);
         window.location.reload();
      } else {
         toast.error(res.EM);
      }
   };

   return (
      <div className="quiz-container">
         <Accordion defaultActiveKey="0">
            <Accordion.Item eventKey="0">
               <Accordion.Header>Manage Quizzes</Accordion.Header>
               <Accordion.Body>
                  <div className="add-new">
                     <fieldset className="border rounded-3 p-3">
                        <legend className="float-none w-auto px-3">Add new quiz:</legend>
                        <div className="form-floating mb-3">
                           <input
                              type="text"
                              className="form-control"
                              placeholder="your quiz's name"
                              value={name}
                              onChange={(event) => setName(event.target.value)}
                           />
                           <label for="floatingInput">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                           <input
                              type="text"
                              className="form-control"
                              placeholder="your quiz's description"
                              value={description}
                              onChange={(event) => setDescription(event.target.value)}
                           />
                           <label for="floatingInput">Description</label>
                        </div>

                        <div className="my-3">
                           <Select
                              defaultValue={type}
                              onChange={setType}
                              options={options}
                              placeholder={"Quiz type..."}
                           />
                        </div>
                        <div className="more-actions form-group">
                           <label className="mb-2">Upload Image</label>
                           <input
                              type="file"
                              className="form-control"
                              // value={image}
                              onChange={(event) => handleUploadImage(event)}
                           />
                        </div>
                        <div className="mt-3">
                           <button className="btn btn-warning" onClick={() => handleAddQuizBtn()}>
                              Save
                           </button>
                        </div>
                     </fieldset>
                  </div>
               </Accordion.Body>
            </Accordion.Item>
         </Accordion>

         <div className="list-detail">
            <TableQuiz />
         </div>
      </div>
   );
};

export default ManageQuizzes;
