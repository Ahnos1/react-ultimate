import React, { useEffect, useState } from "react";
import { getAllQuizForAdmin } from "../../../../services/apiServices";

const TableQuiz = () => {
   const [listQuiz, setListQuiz] = useState([]);

   useEffect(() => {
      fetchQuiz();
   }, []);

   const fetchQuiz = async () => {
      let res = await getAllQuizForAdmin();
      if (res && res.EC === 0) {
         setListQuiz(res.DT);
      }
   };

   return (
      <>
         <div>List Quizzes:</div>
         <table className="table table-hover table-bordered mt-2">
            <thead>
               <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Difficulty</th>
                  <th scope="col">Actions</th>
               </tr>
            </thead>
            <tbody>
               {listQuiz &&
                  listQuiz.map((item, index) => {
                     return (
                        <tr>
                           <th key={`table-quiz-${index}`}>{item.id}</th>
                           <td>{item.name}</td>
                           <td>{item.description}</td>
                           <td>{item.difficulty}</td>
                           <div className="d-flex gap-2">
                              <button className="btn btn-warning">Edit</button>
                              <button className="btn btn-danger">Delete</button>
                           </div>
                        </tr>
                     );
                  })}
            </tbody>
         </table>
      </>
   );
};

export default TableQuiz;
