import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { FcPlus } from "react-icons/fc";
import { toast } from "react-toastify";
import { postCreateNewUsers } from "../../../services/apiServices";

const ModalCreateUser = (props) => {
   const { show, setShow } = props;
   const handleClose = () => {
      setShow(false);
      setEmail("");
      setPassword("");
      setUsername("");
      setRole("USER");
      setImage("");
      setPreviewImage("");
   };
   const handleShow = () => setShow(true);

   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");
   const [username, setUsername] = useState("");
   const [image, setImage] = useState("");
   const [role, setRole] = useState("USER");

   const [previewImage, setPreviewImage] = useState("");

   const handleUpdateImage = (event) => {
      if (event.target && event.target.files && event.target.files[0]) {
         setPreviewImage(URL.createObjectURL(event.target.files[0]));
         setImage(event.target.files[0]);
      } else {
         return 0;
      }
   };

   const validateEmail = (email) => {
      return String(email)
         .toLowerCase()
         .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
         );
   };

   const handleSubmitCreateUser = async () => {
      //validate
      let isValidEmail = validateEmail(email);
      if (!isValidEmail) {
         toast.error("Invalid email");
         return;
      }
      if (!password) {
         toast.error("Invalid password");
         return;
      }
      let res = await postCreateNewUsers(email, password, username, role, image);
      if (res && res.EC === 0) {
         toast.success(res.EM);
         handleClose();
         props.setCurrentPage(1);
         await props.fetchUsersWithPaginate(1);
      }
      if (res && res.EC !== 0) {
         toast.error(res.EM);
      }
   };

   return (
      <>
         <Modal
            show={show}
            onHide={handleClose}
            size="xl"
            backdrop="static"
            className="modal-add-user"
         >
            <Modal.Header closeButton>
               <Modal.Title>Add new user</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <form className="row g-3">
                  <div className="col-md-6">
                     <label className="form-label">Email</label>
                     <input
                        type="email"
                        className="form-control"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                     />
                  </div>
                  <div className="col-md-6">
                     <label className="form-label">Password</label>
                     <input
                        type="password"
                        className="form-control"
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                     />
                  </div>
                  <div className="col-md-6">
                     <label f className="form-label">
                        Username
                     </label>
                     <input
                        type="text"
                        className="form-control"
                        value={username}
                        onChange={(event) => setUsername(event.target.value)}
                     />
                  </div>
                  <div className="col-md-4">
                     <label className="form-label">Role</label>
                     <select
                        className="form-select"
                        value={role}
                        onChange={(event) => setRole(event.target.value)}
                     >
                        <option selected>USER</option>
                        <option>ADMIN</option>
                     </select>
                  </div>
                  <div className="col-12">
                     <label className="label-upload" htmlFor="labelUpload">
                        <FcPlus />
                        Upload File Image
                     </label>
                     <input
                        type="file"
                        hidden
                        id="labelUpload"
                        onChange={(event) => handleUpdateImage(event)}
                     ></input>
                  </div>
                  <div className="col-md-12 img-preview">
                     {previewImage ? <img src={previewImage} /> : <span>Preview Image</span>}
                  </div>
               </form>
            </Modal.Body>
            <Modal.Footer>
               <Button
                  variant="secondary"
                  onClick={() => {
                     handleClose();
                  }}
               >
                  Close
               </Button>
               <Button
                  variant="primary"
                  onClick={() => {
                     handleSubmitCreateUser();
                  }}
               >
                  Save
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
};
export default ModalCreateUser;
