import ModalCreateUser from "./ModalCreateUser";
import "./ManageUser.scss";
import TableUsers from "./TableUsers";
import { useEffect, useState } from "react";
import { getAllUsers, getListUsersWithPaginate } from "../../../services/apiServices";
import ModalUpdateUser from "./ModalUpdateUsers";
import { FcPlus } from "react-icons/fc";
import ModalDeleteUser from "./ModalDeleteUser";
import TableUsersPaginate from "./TableUsersPaginate";

const ManageUsers = () => {
   const LIMIT_USER = 2;
   const [listUsers, setListUsers] = useState([]);
   const [currentPage, setCurrentPage] = useState(1);

   const [pageCount, setPageCount] = useState(0);

   const [showModalUpdateUser, setShowModalUpdateUser] = useState(false);
   const [showModalCreateUser, setShowModalCreateUser] = useState(false);
   const [showModalDeleteUser, setShowModalDeleteUser] = useState(false);
   const [dataUpdate, setDataUpdate] = useState({});
   const [dataDelete, setDataDelete] = useState({});

   useEffect(() => {
      // fetchAllUsers();
      fetchUsersWithPaginate(1);
   }, []);

   const fetchAllUsers = async () => {
      let res = await getAllUsers();
      if (res.EC === 0) {
         setListUsers(res.DT);
      }
   };
   const fetchUsersWithPaginate = async (page) => {
      let res = await getListUsersWithPaginate(page, LIMIT_USER);
      if (res.EC === 0) {
         setListUsers(res.DT.users);
         setPageCount(res.DT.totalPages);
      }
   };

   const handleClickBtnUpdate = (dataUpdate) => {
      setShowModalUpdateUser(true);
      setDataUpdate(dataUpdate);
   };

   const handleResetDataUpdate = () => {
      setDataUpdate({});
   };

   const handleClickBtnDelete = (dataDelete) => {
      setShowModalDeleteUser(true);
      setDataDelete(dataDelete);
   };

   return (
      <div className="manage-user-container">
         <div className="title">Manage Users</div>
         <div className="users-content">
            <div className="btn-add-new">
               <button className="btn btn-primary" onClick={() => setShowModalCreateUser(true)}>
                  <FcPlus></FcPlus>Add new user
               </button>
               <ModalCreateUser
                  show={showModalCreateUser}
                  setShow={setShowModalCreateUser}
                  fetchAllUsers={fetchAllUsers}
                  fetchUsersWithPaginate={fetchUsersWithPaginate}
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
               />
               <ModalUpdateUser
                  show={showModalUpdateUser}
                  setShow={setShowModalUpdateUser}
                  dataUpdate={dataUpdate}
                  fetchAllUsers={fetchAllUsers}
                  handleResetDataUpdate={handleResetDataUpdate}
                  fetchUsersWithPaginate={fetchUsersWithPaginate}
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
               />
               <ModalDeleteUser
                  show={showModalDeleteUser}
                  setShow={setShowModalDeleteUser}
                  dataDelete={dataDelete}
                  fetchAllUsers={fetchAllUsers}
                  fetchUsersWithPaginate={fetchUsersWithPaginate}
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
               />
            </div>

            {/* <TableUsers
               listUsers={listUsers}
               handleClickBtnUpdate={handleClickBtnUpdate}
               handleClickBtnDelete={handleClickBtnDelete}
            />  */}
            <TableUsersPaginate
               listUsers={listUsers}
               pageCount={pageCount}
               handleClickBtnUpdate={handleClickBtnUpdate}
               handleClickBtnDelete={handleClickBtnDelete}
               fetchUsersWithPaginate={fetchUsersWithPaginate}
               currentPage={currentPage}
               setCurrentPage={setCurrentPage}
            />
         </div>
      </div>
   );
};

export default ManageUsers;
