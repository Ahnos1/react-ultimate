import { useState } from "react";
import Select from "react-select";
import "./Questions.scss";
import { FaPlus } from "react-icons/fa";
import { FiMinusCircle } from "react-icons/fi";
import { FaRegTrashAlt } from "react-icons/fa";
import { BsPlusCircleFill } from "react-icons/bs";

import { RiImageAddFill } from "react-icons/ri";
import { v4 as uuidv4 } from "uuid";
import _ from "lodash";

const Questions = () => {
   const options = [
      { value: "chocolate", label: "Chocolate" },
      { value: "strawberry", label: "Strawberry" },
      { value: "vanilla", label: "Vanilla" },
   ];

   const [selectedQuiz, setSelectedQuiz] = useState({});
   const [questions, setQuestions] = useState([
      {
         id: uuidv4(),
         description: "",
         imageFile: null,
         imageName: "",
         answers: [
            {
               id: uuidv4(),
               description: "",
               isCorrect: false,
            },
         ],
      },
   ]);

   const handleAddRemoveQuestion = (type, id) => {
      // console.log(">>Check: ", type, id);
      if (type === "ADD") {
         const newQuestion = {
            id: uuidv4(),
            description: "",
            imageFile: null,
            imageName: "",
            answers: [
               {
                  id: uuidv4(),
                  description: "",
                  isCorrect: false,
               },
            ],
         };
         setQuestions([...questions, newQuestion]);
      }
      if (type === "REMOVE") {
         let questionClone = _.cloneDeep(questions);
         questionClone = questionClone.filter((item) => item.id !== id);
         setQuestions(questionClone);
      }
   };
   const handleAddRemoveAnswer = (type, questionId, answerId) => {
      // console.log(">>Check: ", type, questionId, answerId);
      let questionsClone = _.cloneDeep(questions);
      if (type === "ADD") {
         const newAnswer = {
            id: uuidv4(),
            description: "",
            isCorrect: false,
         };
         let index = questionsClone.findIndex((item) => item.id === questionId);
         questionsClone[index].answers.push(newAnswer);
         setQuestions(questionsClone);
      }
      if (type === "REMOVE") {
         let index = questionsClone.findIndex((item) => item.id === questionId);
         questionsClone[index].answers = questionsClone[index].answers.filter(
            (item) => item.id !== answerId
         );
         setQuestions(questionsClone);
      }
   };

   const handleOnChange = (type, questionId, value) => {
      if (type === "QUESTION") {
         let questionsClone = _.cloneDeep(questions);
         let index = questionsClone.findIndex((item) => item.id === questionId);
         if (index > -1) {
            questionsClone[index].description = value;
            setQuestions(questionsClone);
         }
      }
   };

   const handleOnChangeFileQuestion = (questionId, event) => {
      let questionsClone = _.cloneDeep(questions);
      let index = questionsClone.findIndex((item) => item.id === questionId);
      if (index > -1 && event.target && event.target.files && event.target.files[0]) {
         questionsClone[index].imageFile = event.target.files[0];
         questionsClone[index].imageName = event.target.files[0].name;
         setQuestions(questionsClone);
      }
   };

   const handleOnChangeAnswer = (type, answerId, questionId, value) => {
      let questionsClone = _.cloneDeep(questions);
      let index = questionsClone.findIndex((item) => item.id === questionId);
      if (index > -1) {
         questionsClone[index].answers = questionsClone[index].answers.map((item, index) => {
            if (item.id === answerId) {
               if (type === "CHECKBOX") {
                  item.isCorrect = value;
               }
               if (type === "INPUT") {
                  item.description = value;
               }
            }
            return item;
         });

         setQuestions(questionsClone);
      }
   };

   return (
      <div className="questions-container">
         <div className="title">Manage Questions</div>
         <div className="add-new-question">
            <div className="col-6 form-group">
               <label>Select Quiz:</label>
               <Select defaultValue={selectedQuiz} onChange={setSelectedQuiz} options={options} />
            </div>
            <div className="my-3">Add Question:</div>
            {questions &&
               questions.map((item, index) => {
                  return (
                     <div key={item.id}>
                        <div className="d-flex">
                           <div class="form-floating my-1 col-6">
                              <input
                                 type="text"
                                 class="form-control"
                                 placeholder="name@example.com"
                                 value={item.description}
                                 onChange={(event) =>
                                    handleOnChange("QUESTION", item.id, event.target.value)
                                 }
                              />
                              <label>Question {index + 1}'s description</label>
                           </div>
                           <div className=" ms-3 d-flex justify-content-center align-items-center">
                              <label htmlFor={`${item.id}`}>
                                 <RiImageAddFill
                                    size={22}
                                    color="blue"
                                    title="Add an image"
                                    cursor={"pointer"}
                                    type="file"
                                 />{" "}
                              </label>
                              <input
                                 type="file"
                                 id={`${item.id}`}
                                 hidden
                                 onChange={(event) => handleOnChangeFileQuestion(item.id, event)}
                              />

                              <span className="ms-1 mt-1">
                                 {item.imageName ? item.imageName : "0 image is uploaded"}
                              </span>
                              <div>
                                 <span
                                    className="ms-2"
                                    onClick={() => handleAddRemoveQuestion("ADD", "")}
                                 >
                                    <FaPlus
                                       size={22}
                                       color="green"
                                       title="Add a question"
                                       cursor={"pointer"}
                                    />
                                 </span>
                                 {questions.length > 1 && (
                                    <span
                                       className="ms-2"
                                       onClick={() => handleAddRemoveQuestion("REMOVE", item.id)}
                                    >
                                       <FiMinusCircle
                                          size={22}
                                          color="red"
                                          title="Delete this question"
                                          cursor={"pointer"}
                                       />
                                    </span>
                                 )}
                              </div>
                           </div>
                        </div>
                        {item.answers &&
                           item.answers.length > 0 &&
                           item.answers.map((answer, index) => {
                              return (
                                 <div key={answer.id} className="d-flex mt-4  align-items-center">
                                    <input
                                       checked={answer.isCorrect}
                                       type="checkbox"
                                       className="form-check-input mx-3"
                                       onChange={(event) =>
                                          handleOnChangeAnswer(
                                             "CHECKBOX",
                                             answer.id,
                                             item.id,
                                             event.target.checked
                                          )
                                       }
                                    />
                                    <div class="form-floating my-1 col-6">
                                       <input
                                          type="text"
                                          class="form-control"
                                          placeholder="name@example.com"
                                          value={answer.description}
                                          onChange={(event) =>
                                             handleOnChangeAnswer(
                                                "INPUT",
                                                answer.id,
                                                item.id,
                                                event.target.VALUE
                                             )
                                          }
                                       />
                                       <label>Answer {index + 1}</label>
                                    </div>
                                    <div>
                                       <span
                                          className="ms-2"
                                          onClick={() => handleAddRemoveAnswer("ADD", item.id)}
                                       >
                                          <BsPlusCircleFill
                                             size={22}
                                             color="green"
                                             title="Add a question"
                                             cursor={"pointer"}
                                          />
                                       </span>
                                       {item.answers.length > 1 && (
                                          <span
                                             className="ms-2"
                                             onClick={() =>
                                                handleAddRemoveAnswer("REMOVE", item.id, answer.id)
                                             }
                                          >
                                             <FaRegTrashAlt
                                                size={22}
                                                color="red"
                                                title="Delete this question"
                                                cursor={"pointer"}
                                             />
                                          </span>
                                       )}
                                    </div>
                                 </div>
                              );
                           })}
                     </div>
                  );
               })}
         </div>
         <div className="btn btn-warning mt-3">Save changes</div>
      </div>
   );
};

export default Questions;
