const TableUsers = (props) => {
   const { listUsers } = props;
   return (
      <>
         <table className="table table-bordered table-hover">
            <thead>
               <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Username</th>
                  <th scope="col">Email</th>
                  <th scope="col">Role</th>
                  <th>Actions</th>
               </tr>
            </thead>
            <tbody>
               {listUsers &&
                  listUsers.length > 0 &&
                  listUsers.map((item, index) => {
                     return (
                        <tr key={`table-users-${index}`}>
                           <th scope="row">{item.id}</th>
                           <td>{item.username}</td>
                           <td>{item.email}</td>
                           <td>{item.role}</td>
                           <td>
                              <button className="btn btn-secondary">View</button>
                              <button
                                 className="btn btn-warning mx-3"
                                 onClick={() => props.handleClickBtnUpdate(item)}
                              >
                                 Update
                              </button>
                              <button
                                 className="btn btn-danger"
                                 onClick={() => props.handleClickBtnDelete(item)}
                              >
                                 Delete
                              </button>
                           </td>
                        </tr>
                     );
                  })}
               {listUsers && listUsers.length === 0 && (
                  <tr>
                     <td colSpan={"5"}>Not found user</td>
                  </tr>
               )}
            </tbody>
         </table>
      </>
   );
};
export default TableUsers;
