import ReactPaginate from "react-paginate";
import { useState, useEffect } from "react";

const items = [...Array(33).keys()];

const Items = ({ currentItems }) => {
   return (
      <div className="items">
         {currentItems &&
            currentItems.map((item) => (
               <div>
                  <h3>Item #{item}</h3>
               </div>
            ))}
      </div>
   );
};

const PaginatedItems = ({ itemsPerPage }) => {
   const [pageCount, setPageCount] = useState(0);
   // We start with an empty list of items.
   const [currentItems, setCurrentItems] = useState(null);
   // Here we use item offsets; we could also use page offsets
   // following the API or data you're working with.
   const [itemOffset, setItemOffset] = useState(0);

   useEffect(() => {
      // Fetch items from another resources.
      const endOffset = itemOffset + itemsPerPage;
      console.log(`Loading items from ${itemOffset} to ${endOffset}`);
      setCurrentItems(items.slice(itemOffset, endOffset));
      setPageCount(Math.ceil(items.length / itemsPerPage));
   }, [itemOffset, itemsPerPage]);

   // Invoke when user click to request another page.

   return (
      <>
         <Items currentItems={currentItems} />
      </>
   );
};
const TableUsersPaginate = (props) => {
   const { listUsers, pageCount } = props;

   const handlePageClick = (event) => {
      props.fetchUsersWithPaginate(+event.selected + 1);
      props.setCurrentPage(+event.selected + 1);
      console.log(`User requested page number ${event.selected}`);
   };
   return (
      <>
         <table className="table table-bordered table-hover">
            <thead>
               <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Username</th>
                  <th scope="col">Email</th>
                  <th scope="col">Role</th>
                  <th>Actions</th>
               </tr>
            </thead>
            <tbody>
               {listUsers &&
                  listUsers.length > 0 &&
                  listUsers.map((item, index) => {
                     return (
                        <tr key={`table-users-${index}`}>
                           <th scope="row">{item.id}</th>
                           <td>{item.username}</td>
                           <td>{item.email}</td>
                           <td>{item.role}</td>
                           <td>
                              <button className="btn btn-secondary">View</button>
                              <button
                                 className="btn btn-warning mx-3"
                                 onClick={() => props.handleClickBtnUpdate(item)}
                              >
                                 Update
                              </button>
                              <button
                                 className="btn btn-danger"
                                 onClick={() => props.handleClickBtnDelete(item)}
                              >
                                 Delete
                              </button>
                           </td>
                        </tr>
                     );
                  })}
               {listUsers && listUsers.length === 0 && (
                  <tr>
                     <td colSpan={"5"}>Not found user</td>
                  </tr>
               )}
            </tbody>
         </table>
         <div className="d-flex justify-content-center">
            <ReactPaginate
               nextLabel="next >"
               onPageChange={handlePageClick}
               pageRangeDisplayed={3}
               marginPagesDisplayed={2}
               pageCount={pageCount}
               previousLabel="< previous"
               pageClassName="page-item"
               pageLinkClassName="page-link"
               previousClassName="page-item"
               previousLinkClassName="page-link"
               nextClassName="page-item"
               nextLinkClassName="page-link"
               breakLabel="..."
               breakClassName="page-item"
               breakLinkClassName="page-link"
               containerClassName="pagination"
               activeClassName="active"
               renderOnZeroPageCount={null}
               forcePage={props.currentPage - 1}
            />
         </div>
      </>
   );
};
export default TableUsersPaginate;
