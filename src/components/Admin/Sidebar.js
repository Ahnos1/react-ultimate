import "../Admin/Sidebar.scss";
import {
   ProSidebar,
   Menu,
   MenuItem,
   SubMenu,
   SidebarHeader,
   SidebarFooter,
   SidebarContent,
} from "react-pro-sidebar";
import { FaTachometerAlt, FaGithub } from "react-icons/fa";
import { DiReact } from "react-icons/di";
import sidebarBg from "../../assets/bg2.jpg";
import { IoDiamondOutline } from "react-icons/io5";
import { Link, useNavigate } from "react-router-dom";

const Sidebar = ({ image, collapsed, toggled, handleToggleSidebar }) => {
   const navigate = useNavigate();
   return (
      <>
         <ProSidebar
            image={sidebarBg}
            collapsed={collapsed}
            toggled={toggled}
            breakPoint="md"
            onToggle={handleToggleSidebar}
         >
            <SidebarHeader>
               <div
                  style={{
                     padding: "24px",
                     textTransform: "uppercase",
                     fontWeight: "bold",
                     fontSize: 14,
                     letterSpacing: "1px",
                     overflow: "hidden",
                     textOverflow: "ellipsis",
                     whiteSpace: "nowrap",
                  }}
               >
                  <DiReact
                     style={{ cursor: "pointer" }}
                     onClick={() => navigate("/")}
                     size={"3em"}
                     color="#00ffff"
                  />
                  <span style={{ cursor: "pointer" }} onClick={() => navigate("/")}>
                     HOI DAN IT{" "}
                  </span>
               </div>
            </SidebarHeader>

            <SidebarContent>
               <Menu iconShape="circle">
                  <MenuItem
                     icon={<FaTachometerAlt />}
                     // suffix={<span className="badge red">NEW</span>}
                  >
                     {" "}
                     <Link to="/admin" />
                     Dashboard
                  </MenuItem>
                  {/* <MenuItem icon={<FaGem />}> Components</MenuItem> */}
               </Menu>
               <Menu iconShape="circle">
                  <SubMenu
                     // suffix={<span className="badge yellow">3</span>}
                     icon={<IoDiamondOutline />}
                     title={"Features"}
                  >
                     <MenuItem>
                        <Link to="/admin/manage-users" />
                        Quản lý user
                     </MenuItem>
                     <MenuItem>
                        <Link to="/admin/manage-quizzes" /> Quản lý bài quiz
                     </MenuItem>
                     <MenuItem>
                        <Link to="/admin/manage-questions" /> Quản lý câu hỏi
                     </MenuItem>
                  </SubMenu>
               </Menu>
            </SidebarContent>

            <SidebarFooter style={{ textAlign: "center" }}>
               <div
                  className="sidebar-btn-wrapper"
                  style={{
                     padding: "20px 24px",
                  }}
               >
                  <a
                     href="https://github.com/azouaoui-med/react-pro-sidebar"
                     target="_blank"
                     className="sidebar-btn"
                     rel="noopener noreferrer"
                  >
                     <FaGithub />
                     <span
                        style={{
                           whiteSpace: "nowrap",
                           textOverflow: "ellipsis",
                           overflow: "hidden",
                        }}
                     >
                        viewSource
                     </span>
                  </a>
               </div>
            </SidebarFooter>
         </ProSidebar>
      </>
   );
};

export default Sidebar;
