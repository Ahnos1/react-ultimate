import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { NavLink, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

const Header = () => {
   const account = useSelector((state) => state.user.account);
   const isAuthenticated = useSelector((state) => state.user.isAuthenticated);
   const navigate = useNavigate();

   const handleLogin = () => {
      navigate("/login");
   };

   const handleGoToSignUp = () => {
      navigate("/register");
   };

   return (
      <Navbar expand="lg" className="bg-body-tertiary">
         <Container>
            {/* <Navbar.Brand href="/">Hỏi Dân IT</Navbar.Brand> */}
            <NavLink to="/" className="navbar-brand">
               Hỏi Dân IT
            </NavLink>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
               <Nav className="me-auto">
                  <NavLink to="/" className="nav-link">
                     Home
                  </NavLink>
                  <NavLink to="/users" className="nav-link">
                     User
                  </NavLink>
                  <NavLink to="/admin" className="nav-link">
                     Admin
                  </NavLink>
               </Nav>
               <Nav>
                  {isAuthenticated === false ? (
                     <>
                        <button className="btn-login " onClick={() => handleLogin()}>
                           Log in
                        </button>
                        <button className="btn-signup" onClick={() => handleGoToSignUp()}>
                           Sign up
                        </button>
                     </>
                  ) : (
                     <NavDropdown title="Settings" id="basic-nav-dropdown">
                        <NavDropdown.Item>Profile</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item>Logout</NavDropdown.Item>
                     </NavDropdown>
                  )}
               </Nav>
            </Navbar.Collapse>
         </Container>
      </Navbar>
   );
};

export default Header;
