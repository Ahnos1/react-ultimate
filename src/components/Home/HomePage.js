import videoHomepage from "../../assets/video-homepage.mp4";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
const HomePage = (props) => {
   const navigate = useNavigate();
   const isAuthenticated = useSelector((state) => state.user.isAuthenticated);
   return (
      <>
         <div className="homepage-container">
            <video autostart autoPlay muted loop>
               <source src={videoHomepage} type="video/mp4" />
            </video>
         </div>
         <div className="homepage-content">
            <div>There's a better way to ask </div>
            <div>
               You don't want to make a boring form. And your audiance won't answer one. Create a
               typeform instead-and make everyone happy.
            </div>
            {isAuthenticated === false ? (
               <div>
                  <button onClick={() => navigate("/login")}>Get started. It's free</button>
               </div>
            ) : (
               <div>
                  <button
                     className="btn btn-dark"
                     onClick={() => {
                        navigate("/users");
                     }}
                  >
                     Doing quiz now
                  </button>
               </div>
            )}
         </div>
      </>
   );
};

export default HomePage;
