import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import User from "./components/User/User";
import Admin from "./components/Admin/Admin";
import HomePage from "./components/Home/HomePage";
import "react-pro-sidebar/dist/css/styles.css";
import ManageUsers from "./components/Admin/Content/ManageUsers";
import Dashboard from "./components/Admin/Content/Dashboard";
import Login from "./components/Auth/Login";
import App from "./App";
import { Bounce, ToastContainer } from "react-toastify";
import Register from "../src/components/Auth/Register";
import ListQuiz from "./components/User/ListQuiz";
import DetailQuiz from "./components/User/DetailQuiz";
import ManageQuizzes from "./components/Admin/Content/Quiz/ManageQuizzes";
import Questions from "./components/Admin/Content/Question/Questions";

const Layout = () => {
   const NotFound = () => {
      return <div className="container mt-3 alert alert-danger">404 Not Found</div>;
   };
   return (
      <>
         <Routes>
            <Route path="/" element={<App />}>
               <Route index element={<HomePage />}></Route>
               <Route path="users" element={<ListQuiz />}></Route>
            </Route>
            <Route path="/quiz/:id" element={<DetailQuiz />}></Route>
            <Route path="/admin" element={<Admin />}>
               <Route index element={<Dashboard />}></Route>
               <Route path="manage-users" element={<ManageUsers />}></Route>
               <Route path="manage-quizzes" element={<ManageQuizzes />}></Route>
               <Route path="manage-questions" element={<Questions />}></Route>
            </Route>
            <Route path="/login" element={<Login />}></Route>
            <Route path="/register" element={<Register />}></Route>
            <Route path="/*" element={<NotFound />}></Route>
         </Routes>
         <ToastContainer
            position="bottom-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light"
            transition={Bounce}
         />
      </>
   );
};

export default Layout;
